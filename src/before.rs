#[cfg(test)]
mod tests {
    use super::*;
    use serde_json::json;

    #[test]
    fn test_get_current_weather() {
        let weather_tool_json = r#"
        {
            "name": "get_current_weather",
            "description": "Get the current weather in a given location",
            "parameters": {
                "type": "object",
                "properties": {
                    "location": {
                        "type": "string",
                        "description": "The city and state, e.g. San Francisco, CA"
                    },
                    "unit": {
                        "type": "string",
                        "enum": ["celsius", "fahrenheit"],
                        "description": "The unit of measurement"
                    }
                },
                "required": ["location", "unit"]
            }
        }
        "#;

        let llm_output = json!({
            "arguments": [
                "location": "sfo",
                "unit": "celsius"
            ]
        });

/* blah balsh something to be ignored
 */
let tool = create_tool_with_function!(fn get_current_weather(location: String, unit: String) -> String, weather_tool_json);
    let result = tool.call(llm_output).expect("Function call failed");
println!("{result:?}");
assert_eq!(result, "Weather for Glasgow, Scotland in celsius");
}
}